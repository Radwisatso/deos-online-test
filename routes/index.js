const router = require("express").Router();
const postRoute = require('./postRoute');
const tagRoute = require('./tagRoute');

router.use('/post', postRoute);
router.use('/tag', tagRoute)

module.exports = router;
