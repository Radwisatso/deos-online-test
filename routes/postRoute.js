const router = require("express").Router();
const PostController = require("../controllers/PostController");

router.get('/', PostController.getData);
router.get('/:id', PostController.getOneData);
router.post('/add', PostController.addData);
router.patch('/update/:id', PostController.updateData);
router.delete('/delete/:id', PostController.deleteData);

module.exports = router;