const router = require("express").Router();
const TagController = require("../controllers/TagController");

router.get('/', TagController.getData);
router.get('/:id', TagController.getOneData);
router.post('/add', TagController.addData);
router.patch('/update/:id', TagController.updateData);
router.delete('/delete/:id', TagController.deleteData);

module.exports = router;