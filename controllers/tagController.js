const { Post, PostTag, Tag } = require("../models")

class TagController {

    static async getData(req, res) {
        try {
            const result = await Tag.findAll({
                include: [
                    {
                        model: Post,
                        as: 'posts',
                        through: PostTag
                    },
                ],
                attributes: {
                    exclude: ['createdAt', 'updatedAt']
                }
            });
            if (!result) {
                res.status(404).json('Tags not found');
            } else {
                res.status(200).json(result);
            }
        } catch (error) {
            res.status(500).json(error);
        }
    }

    static async getOneData(req, res) {
        const { id } = req.params
        try {
            const result = await Tag.findByPk(id, {
                include: [
                    {
                        model: Post,
                        as: 'posts',
                        through: PostTag
                    },
                ],
                attributes: {
                    exclude: ['createdAt', 'updatedAt']
                }
            });
            if (!result) {
                res.status(404).json('Tag not found');
            } else {
                res.status(200).json(result);
            }
        } catch (error) {
            res.status(500).json(error);
        }
    }

    static async addData(req, res) {
        try {
            const result = await Tag.create({
                label: req.body.label
            })
            res.status(201).json(result)
        } catch (error) {
            res.status(500).json(error)
        }
    }

    static async updateData(req, res) {
        const { id } = req.params
        try {
            const findTag = await Tag.findByPk(id);
            if (!findTag) {
                res.status(404).json('Tag not found')
            } else {
                const result = await Tag.update({
                    label: req.body.label,
                }, {
                    where: {
                        id: id
                    },
                    returning: true
                })
                res.status(200).json(result[1][0])
            }
        } catch (error) {
            res.status(500).json(error)
        }
    }

    static async deleteData(req, res) {
        const { id } = req.params
        try {
            const findTag = await Tag.findByPk(id);
            if (!findTag) {
                res.status(404).json('Tag not found')
            } else {
                await Tag.destroy({
                    where: {
                        id: id
                    }
                })
                await PostTag.destroy({
                    where: {
                        TagId: id
                    }
                })
                res.status(200).json(`Successfully deleted tag with id: ${id}`)
            }
        } catch (error) {
            res.status(500).json(error)
        }
    }

};

module.exports = TagController;