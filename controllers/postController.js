
const { Post, PostTag, Tag } = require("../models")

class PostController {

    static async getData(req, res) {
        try {
            const result = await Post.findAll({
                include: [
                    {
                        model: Tag,
                        as: 'tags',
                        through: PostTag
                    },
                ],
                attributes: {
                    exclude: ['createdAt', 'updatedAt']
                }
            });
            if (!result) {
                res.status(404).json('Posts not found');
            } else {
                res.status(200).json(result);
            }
        } catch (error) {
            res.status(500).json(error);
        }
    }

    static async getOneData(req, res) {
        const { id } = req.params
        try {
            const result = await Post.findByPk(id, {
                include: [
                    {
                        model: Tag,
                        as: 'tags',
                        through: PostTag
                    },
                ],
                attributes: {
                    exclude: ['createdAt', 'updatedAt']
                }
            });
            if (!result) {
                res.status(404).json('Post not found');
            } else {
                res.status(200).json(result);
            }
        } catch (error) {
            res.status(500).json(error);
        }
    }

    static async addData(req, res) {
        const tags_id = req.query.tags_id
        try {
            const createdPost = await Post.create({
                title: req.body.title,
                content: req.body.content
            });
            const postTags = (tags) => tags.map((value, index) => {
                return {
                    PostId: createdPost.id,
                    TagId: value
                }
            })

            const result = await PostTag.bulkCreate(postTags(tags_id), {
                returning: true,
                updateOnDuplicate: ['PostId', 'TagId']
            })

            res.status(201).json({
                post: createdPost,
                relation: result
            })
        } catch (error) {
            res.status(500).json(error)
        }
    }

    static async updateData(req, res) {
        const { id } = req.params
        try {
            const findPost = await Post.findByPk(id);
            if (!findPost) {
                res.status(404).json('Post not found')
            } else {
                const result = await Post.update({
                    title: req.body.title,
                    content: req.body.content
                }, {
                    where: {
                        id: id
                    },
                    returning: true
                })

                res.status(200).json(result[1][0])
            }
        } catch (error) {
            res.status(500).json(error)
        }
    }

    static async deleteData(req, res) {
        const { id } = req.params
        try {
            const findPost = await Post.findByPk(id);
            if (!findPost) {
                res.status(404).json('Post not found')
            } else {
                await Post.destroy({
                    where: {
                        id: id
                    },
                })
                await PostTag.destroy({
                    where: {
                        PostId: id
                    },
                })
                res.status(200).json(`Successfully deleted post with id: ${id}`)
            }
        } catch (error) {
            res.status(500).json(error)
        }
    }

};

module.exports = PostController;