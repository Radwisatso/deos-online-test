'use strict';

module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.bulkInsert('Tags', [
      {
        // id: 1,
        label: 'Framework',
        posts: null,
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        // id: 2,
        label: 'ORM',
        posts: null,
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        // id: 3,
        label: 'Library',
        posts: null,
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        // id: 4,
        label: 'Environment',
        posts: null,
        createdAt: new Date(),
        updatedAt: new Date()
      },
    ], {})
    /**
     * Add seed commands here.
     *
     * Example:
     * await queryInterface.bulkInsert('People', [{
     *   name: 'John Doe',
     *   isBetaMember: false
     * }], {});
    */
  },

  down: async (queryInterface, Sequelize) => {
    await queryInterface.bulkDelete('Tags', null, {})
    /**
     * Add commands to revert seed here.
     *
     * Example:
     * await queryInterface.bulkDelete('People', null, {});
     */
  }
};
