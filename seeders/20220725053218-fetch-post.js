'use strict';

module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.bulkInsert('Posts', [
      {
        // id: 1,
        title: 'Creating server-side rendering with express',
        content: 'Lorem ipsum dolor sit amet',
        tags: null,
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        // id: 2,
        title: 'Integrate database using postgresql and sequelize orm',
        content: 'Lorem ipsum dolor sit amet',
        tags: null,
        createdAt: new Date(),
        updatedAt: new Date()
      }
    ], {})
    /**
     * Add seed commands here.
     *
     * Example:
     * await queryInterface.bulkInsert('People', [{
     *   name: 'John Doe',
     *   isBetaMember: false
     * }], {});
    */
  },

  down: async (queryInterface, Sequelize) => {
    await queryInterface.bulkDelete('Posts', null, {})
    /**
     * Add commands to revert seed here.
     *
     * Example:
     * await queryInterface.bulkDelete('People', null, {});
     */
  }
};
