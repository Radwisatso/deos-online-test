# DEOS Online Test



## Introduction

This is a documentation for DEOS online user interview test
<br />
<br />

# Posts

## Get Post (GET)

### Success (200)

```
[
    {
        "id": 11,
        "title": "Lorem ipsum",
        "content": "Dolor sit amet",
        "tags": [
            {
                "id": 1,
                "label": "Framework",
                "createdAt": "2022-07-25T07:31:59.292Z",
                "updatedAt": "2022-07-25T07:31:59.292Z",
                "PostTags": {
                    "createdAt": "2022-07-25T14:22:24.329Z",
                    "updatedAt": "2022-07-25T14:22:24.329Z",
                    "PostId": 11,
                    "TagId": 1
                }
            },
            {
                "id": 2,
                "label": "ORM",
                "createdAt": "2022-07-25T07:31:59.292Z",
                "updatedAt": "2022-07-25T07:31:59.292Z",
                "PostTags": {
                    "createdAt": "2022-07-25T14:22:24.329Z",
                    "updatedAt": "2022-07-25T14:22:24.329Z",
                    "PostId": 11,
                    "TagId": 2
                }
            }
        ]
    },
    more...
]
```
## Get One Post (GET)

### Success (200)

```
{
    "id": 11,
    "title": "Lorem ipsum",
    "content": "Dolor sit amet",
    "tags": [
        {
            "id": 1,
            "label": "Framework",
            "createdAt": "2022-07-25T07:31:59.292Z",
            "updatedAt": "2022-07-25T07:31:59.292Z",
            "PostTags": {
                "createdAt": "2022-07-25T14:22:24.329Z",
                "updatedAt": "2022-07-25T14:22:24.329Z",
                "PostId": 11,
                "TagId": 1
            }
        },
        {
            "id": 2,
            "label": "ORM",
            "createdAt": "2022-07-25T07:31:59.292Z",
            "updatedAt": "2022-07-25T07:31:59.292Z",
            "PostTags": {
                "createdAt": "2022-07-25T14:22:24.329Z",
                "updatedAt": "2022-07-25T14:22:24.329Z",
                "PostId": 11,
                "TagId": 2
            }
        }
    ]
}
```

## Add Post (POST)

### Success (201)

```
{
    "post": {
        "id": 12,
        "title": "Lorem ipsum",
        "content": "Dolor sit amet",
        "updatedAt": "2022-07-25T14:22:35.740Z",
        "createdAt": "2022-07-25T14:22:35.740Z"
    },
    "relation": [
        {
            "id": 45,
            "PostId": 12,
            "TagId": 3,
            "createdAt": "2022-07-25T14:22:35.799Z",
            "updatedAt": "2022-07-25T14:22:35.799Z"
        },
        {
            "id": 46,
            "PostId": 12,
            "TagId": 4,
            "createdAt": "2022-07-25T14:22:35.799Z",
            "updatedAt": "2022-07-25T14:22:35.799Z"
        }
    ]
}
```
## Update Post (PATCH)

### Success (200)

```
{
    "id": 7,
    "title": "Updated title 1",
    "content": "Updated content 1",
    "createdAt": "2022-07-25T07:41:36.887Z",
    "updatedAt": "2022-07-25T13:47:21.320Z"
}
```
## Delete Post (DELETE)

### Success (200)

```
"Successfully deleted post with id: 10"
```
<br/>

# Tag
## Get Tag (GET)

### Success (200)

```
[
    {
        "id": 1,
        "label": "Framework",
        "posts": [
            {
                "id": 11,
                "title": "Lorem ipsum",
                "content": "Dolor sit amet",
                "createdAt": "2022-07-25T14:22:24.168Z",
                "updatedAt": "2022-07-25T14:22:24.168Z",
                "PostTags": {
                    "createdAt": "2022-07-25T14:22:24.329Z",
                    "updatedAt": "2022-07-25T14:22:24.329Z",
                    "PostId": 11,
                    "TagId": 1
                }
            }
        ]
    },
    more...
]
```
## Get One Tag (GET)

### Success (200)

```
{
    "id": 1,
    "label": "Framework",
    "posts": [
        {
            "id": 11,
            "title": "Lorem ipsum",
            "content": "Dolor sit amet",
            "createdAt": "2022-07-25T14:22:24.168Z",
            "updatedAt": "2022-07-25T14:22:24.168Z",
            "PostTags": {
                "createdAt": "2022-07-25T14:22:24.329Z",
                "updatedAt": "2022-07-25T14:22:24.329Z",
                "PostId": 11,
                "TagId": 1
            }
        }
    ]
}
```
## Add Tag (POST)

### Success (201)

```
{
    "id": 6,
    "label": "Database",
    "updatedAt": "2022-07-25T13:59:02.916Z",
    "createdAt": "2022-07-25T13:59:02.916Z"
}
```
## Update Tag (PATCH)

### Success (200)

```
{
    "id": 5,
    "label": "Database updated",
    "createdAt": "2022-07-25T13:59:02.304Z",
    "updatedAt": "2022-07-25T14:10:40.919Z"
}
```
## Delete Tag (PATCH)

### Success (200)

```
"Successfully deleted tag with id: 5, and posttag with TagId: 5"
```

<br/>

# Error handler
## Post not found (404)

```
"Posts not found"
```
## Default server error (500)

```
{} || "Internal server error"
```